import React from 'react'
import {Button, Card, Spin, Typography} from "antd";
import {Link} from "react-router-dom";
import {
    RedoOutlined,
    PlayCircleOutlined,
    StepForwardOutlined,
    PauseCircleOutlined
} from '@ant-design/icons';
import {editDisplayName, IDisplay} from "../api";
import {getPlaceholderImage} from "../../../utils/getPlaceholderImage";
import {LoadingStatus} from "../../../utils/fetchWIthRetry";
import {useWebSocket} from "../../../pages/Dashboard/components/WSProvider";


const DisplayCard = (props: IDisplay) => {
    const [name, setName] = React.useState<string>(props.name)
    const [nameEditLoadingStatus, setNameEditLoadingStatus] = React.useState<LoadingStatus>('idle')
    const {sendMessage} = useWebSocket()

    const sendSignal = (data: any) => {
        sendMessage(JSON.stringify({
            type: 'signal',
            displayId: props.id,
            ...data
        }))
    }

    const actions = {
        offline: [],
        playing: [
            <RedoOutlined onClick={() => sendSignal({signal: 'restart'})}/>,
            <PauseCircleOutlined onClick={() => sendSignal({signal: 'pause'})}/>,
            <StepForwardOutlined onClick={() => sendSignal({signal: 'next'})}/>
        ],
        paused: [
            <RedoOutlined onClick={() => sendSignal({signal: 'restart'})}/>,
            <PlayCircleOutlined onClick={() => sendSignal({signal: 'resume'})}/>,
            <StepForwardOutlined onClick={() => sendSignal({signal: 'next'})}/>
        ],
        stopped: [
            <PlayCircleOutlined onClick={() => sendSignal({signal: 'run'})}/>,
        ],
        unknown: []
    }

    return (
        <Card
            cover={
                <div style={{height: '150px', width: '100%'}}>
                    <img
                        alt="preview"
                        src={props.preview || getPlaceholderImage(props.id)}
                        style={{width: '100%', height: '100%', objectFit: 'cover'}}/>
                </div>
            }

            actions={actions[props.status] as React.ReactNode[]}
        >
            <Card.Meta
                title={(<>
                    <Typography.Text editable={{
                        icon: nameEditLoadingStatus === 'loading' ? (<Spin />) : undefined,
                        tooltip: 'Изменить имя дисплея',
                        maxLength: 50,
                        onChange: (value) => {
                            editDisplayName(setNameEditLoadingStatus, {...props, name: value})
                                .then(_ => { setName(value) })
                        }
                    }} >{name}</Typography.Text>
                </>)}
                description={(<Link to={'/'}>{ props.schedulerName }</Link>)} />
        </Card>
    )
}

export default DisplayCard
