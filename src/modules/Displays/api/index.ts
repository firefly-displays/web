import {fetchWithRetry, StatusSetter} from "../../../utils/fetchWIthRetry";

const base_url = process.env.REACT_APP_SERVER_URL

export type DisplayStatus = 'offline' | 'playing' | 'paused' | 'stopped' | 'unknown'

export interface IDisplay {
    id: string,
    name: string,
    preview: string,
    schedulerId: string | null,
    schedulerName: string | null,
    status: DisplayStatus
}

export async function fetchDisplays(hostId: string, statusSetter: StatusSetter): Promise<IDisplay[] | null> {
    const qp = new URLSearchParams({
        host: hostId
    })
    return await fetchWithRetry<IDisplay[]>(`${base_url}/api/displays?${qp.toString()}`, {statusSetter})
}

export async function editDisplayName(statusSetter: StatusSetter, display: IDisplay) {

    const all = await fetchWithRetry<IDisplay[]>(`${base_url}/displays`, {statusSetter})

    // TODO подправить логику под реальный сервер
    const payload = all && all.map(d => d.id !== display.id ? d : display)

    return fetchWithRetry(`${base_url}/displays`, {
        method: 'PATCH',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
        statusSetter
    })
}