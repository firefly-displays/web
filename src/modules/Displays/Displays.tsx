import React, {useEffect} from 'react'
import {Button, Col, Empty, Row, Typography, Spin} from 'antd';
import DisplayCard from "./components/DisplayCard";
import {fetchDisplays, IDisplay} from "./api";
import {LoadingStatus} from "../../utils/fetchWIthRetry";
import {useOutletContext} from "react-router-dom";
import {useWebSocket} from "../../pages/Dashboard/components/WSProvider";

const Displays = () => {
    const {hostId} = useOutletContext<{hostId: string}>();

    const [
        displays,
        setDisplays
    ] = React.useState<IDisplay[] | null>(null)

    React.useEffect(() => {
        fetchDisplays(hostId, setDisplaysLoadingStatus)
            .then(data => setDisplays(data))
    }, [])

    const [displaysLoadingStatus, setDisplaysLoadingStatus] = React.useState<LoadingStatus>('idle')
    const { sendMessage } = useWebSocket()

    return (
        <div>
            <Row justify="space-between" align="middle">
                <Typography.Title level={2}>Дисплеи</Typography.Title>
                {displaysLoadingStatus === 'ok' && <Button
                    onClick={() => {
                        sendMessage(JSON.stringify({
                            type: 'signal',
                            signal: 'identify'
                        }))
                    }}
                >Определить</Button>}
            </Row>

            {displaysLoadingStatus === 'loading' && (
                <Spin />
            )}

            {displaysLoadingStatus === 'ok' && (
                <Row gutter={[16, 16]}>
                    {displays && displays.map((display, index) => (
                        <Col key={index} xs={24} sm={24} md={24} lg={12} xl={12}>
                            <DisplayCard {...display} />
                        </Col>
                    ))}
                </Row>
            )}

            {displaysLoadingStatus === 'failed' && (
                <Empty description={'Не удалось загрузить информацию о дисплеях'} />
            )}
        </div>
    )
}

export default Displays
