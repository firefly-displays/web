import React from 'react';
import { storiesOf } from '@storybook/react';
import TimeLine from './Timeline';

storiesOf('Schedulers/TimeLine', module)
    .add('Default', () => <TimeLine />);

export default {
    title: 'Schedulers/TimeLine',
    component: TimeLine,
};
