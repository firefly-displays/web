import React from 'react'
import {Link, useParams} from "react-router-dom";
import {Breadcrumb, Button, List, Space, Typography} from "antd";
import cronHumanize from "../../../utils/cronHumanize";
import { DownOutlined, UpOutlined, DeleteOutlined } from '@ant-design/icons';

interface scheduledItem {
    id: string,
    name: string,
    cron: string,
    duration: number
}

const Scheduler = () => {
    const { id } = useParams();

    const data = {
        name: "Главный планировщик",
        playlists: [
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Кулинарные рецепты',
                cron: '0 0 10,11,12 ? * * *',
                duration: 60*60*2,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Туристические приключения',
                cron: '0 0 0 1 1 * *',
                duration: 60*30,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Научно-популярные документальные фильмы',
                cron: '0 30 12/2 * * ? *',
                duration: 60*60*4,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Спортивные моменты',
                cron: '0 10 10 * * 2 2022-2024',
                duration: 60*60,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Юмористические шоу',
                cron: '0 20 10-13 ? * MON,FRI *',
                duration: 60*60*2,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Исторические документальные фильмы',
                cron: '0 30 12/2 * * ? *',
                duration: 60*60*10,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Уроки фотографии',
                cron: '0 20 8 ? * 2L *',
                duration: 60*60*2,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Игровые обзоры',
                cron: '0 40 * ? * * *',
                duration: 60*60*2,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Технологические новости',
                cron: '0 20,30,45 * ? * * *',
                duration: 60*60,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Природные красоты',
                cron: '0 30 * ? * * *',
                duration: 60*45,
            },
            {
                id: '5d779b06-6f72-40b2-a28b-52ed1faa85ea',
                name: 'Животные и дикие природные явления',
                cron: '0 0 10,11,12 ? * * *',
                duration: 60*60*5,
            },
        ]
    }

    return (
        <>
            <Breadcrumb
                items={[
                    { title: <Link to={'/scheduler/'}>Планировщики</Link> },
                    { title: `${data.name}` },
                ]}
            />
            <Typography.Title editable>{data.name}</Typography.Title>
            <List
                pagination={{ position: 'bottom', align: 'start' }}
                dataSource={data.playlists}
                size="large"
                renderItem={(item, index) => (<ListItem {...item}/>)}
            />
        </>
    )
}

const ListItem = (item: scheduledItem) => {
    const [hovered, setHovered] = React.useState(false)

    return (
        <List.Item
            onMouseOver={() => setHovered(true)}
            onMouseOut={() => setHovered(false)}
        >
            <List.Item.Meta
                title={(<Link to={`/playlists/${item}`}>{item.name}</Link>)}
                description={cronHumanize.humanize(item.cron)}
                avatar={(
                    <Space direction="vertical" size="small" style={{ display: 'flex', width: '24px' }}>
                        <Button type={'text'} size={'small'}><Typography.Text type={'secondary'}>
                            {hovered && (<UpOutlined />)}
                        </Typography.Text></Button>

                        <Button type={'text'} size={'small'}><Typography.Text type={'secondary'}>
                            {hovered && (<DownOutlined />)}
                        </Typography.Text></Button>
                    </Space>
                )}
            />
            {hovered && (
                <Button type={'text'} size={'small'}><Typography.Text type={'danger'}>
                    <DeleteOutlined />
                </Typography.Text></Button>
            )}
        </List.Item>
    )
}

export default Scheduler
