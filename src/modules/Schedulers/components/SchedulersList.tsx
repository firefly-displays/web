import React, {useEffect, useState} from 'react'
import {Button, Form, Input, List, Modal, Row, Typography} from "antd";
import {Link, useLocation, useOutletContext} from "react-router-dom";
import {PlusOutlined} from "@ant-design/icons";
import {LoadingStatus} from "../../../utils/fetchWIthRetry";
import {fetchSchedulers, IScheduler, postScheduler} from "../api/api";

const SchedulersList = () => {
    const {hostId} = useOutletContext<{hostId: string}>();
    const location = useLocation();

    const [
        shedulers,
        setSchedulers
    ] = React.useState<IScheduler[] | null>(null)

    React.useEffect(() => {
        fetchSchedulers(hostId, setSchedulersLoadingStatus)
            .then(data => setSchedulers(data))
    }, [])

    const [schedulersLoadingStatus, setSchedulersLoadingStatus] = React.useState<LoadingStatus>('idle')

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [form] = Form.useForm();

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        form
            .validateFields()
            .then(values => {
                postScheduler(hostId, values)
                    .then(data => {
                        setIsModalVisible(false);
                        if (data) {
                            setSchedulers(prev => prev ? [...prev, data] : [data])
                        }
                        form.resetFields();
                    })
                    .catch(() => {});
            })
            .catch(() => {});
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };


    return (
        <>
            <Typography.Title level={2}>Планировщики</Typography.Title>
            <Row justify="space-between" align="middle">
                <Button icon={<PlusOutlined />} onClick={showModal} >Создать</Button>
                <Modal title="Создание планировщика" open={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <Form form={form} layout="vertical" name="form_in_modal">
                        <Form.Item
                            name="name"
                            label="Имя планировщика"
                            rules={[{ required: true, message: 'Укажите имя планировщика' }]}
                        >
                            <Input />
                        </Form.Item>
                    </Form>
                </Modal>
            </Row>
            <List
                pagination={{ position: 'bottom', align: 'start' }}
                dataSource={shedulers || []}
                size="large"
                renderItem={(item, index) => (
                    <List.Item>
                        <Link to={`${location.pathname}/${item.id}`}>
                            <Typography.Text>{item.name}</Typography.Text>
                        </Link>
                    </List.Item>
                )}
            />
        </>
    )
}

export default SchedulersList
