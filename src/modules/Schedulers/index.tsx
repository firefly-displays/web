import React from 'react'
import {Outlet, useOutletContext} from "react-router-dom";

const Index = () => {
    const {hostId} = useOutletContext<{hostId: string}>();

    return (
        <div>
            <Outlet context={{hostId: hostId}} />
        </div>
    )
}

export default Index

export { default as Scheduler } from './components/Scheduler'
export { default as SchedulersList } from './components/SchedulersList'