import {fetchWithRetry, StatusSetter} from "../../../utils/fetchWIthRetry";

const base_url = process.env.REACT_APP_SERVER_URL

export interface IScheduler {
    id: string,
    name: string
}

export async function fetchSchedulers(hostId: string, statusSetter: StatusSetter): Promise<IScheduler[] | null> {
    const qp = new URLSearchParams({
        host: hostId
    })
    return await fetchWithRetry<IScheduler[]>(`${base_url}/api/schedulers?${qp.toString()}`, {statusSetter})
}

export async function postScheduler(hostId: string, values: any) {
    const qp = new URLSearchParams({
        host: hostId
    })
    return fetchWithRetry<IScheduler>(`${base_url}/api/schedulers?${qp.toString()}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
    })
}

// export async function editDisplayName(statusSetter: StatusSetter, display: IDisplay) {
//
//     const all = await fetchWithRetry<IDisplay[]>(`${base_url}/displays`, {statusSetter})
//
//     // TODO подправить логику под реальный сервер
//     const payload = all && all.map(d => d.id !== display.id ? d : display)
//
//     return fetchWithRetry(`${base_url}/displays`, {
//         method: 'PATCH',
//         headers: {
//             "Content-Type": "application/json",
//         },
//         body: JSON.stringify(payload),
//         statusSetter
//     })
// }