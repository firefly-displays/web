import React from 'react'
import {Outlet, useOutletContext} from "react-router-dom";

const Index = () => {
    const {hostId} = useOutletContext<{hostId: string}>();

    return (
        <div>
            <Outlet context={{hostId: hostId}} />
        </div>
    )
}

export default Index

export { default as Playlist } from './components/Playlist'
export { default as PlaylistsList } from './components/PlaylistsList'