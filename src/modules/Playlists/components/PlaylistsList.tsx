import React, {useEffect, useState} from 'react'
import {Button, Form, Input, List, Modal, Row, Typography} from "antd";
import {PlusOutlined} from "@ant-design/icons";
import {Link, useLocation, useOutletContext} from "react-router-dom";
import {fetchSchedulers, IScheduler, postScheduler} from "../../Schedulers/api/api";
import {fetchPlaylists} from "../api";
import {fetchMedia} from "../../Media/api";

const PlaylistsList = () => {
    let data = [
        {
            id: 'fda72d70-221c-4098-8eaa-43b40c9be15c',
            name: 'Первый плейлист',
        },
        {
            id: '364136ce-c83d-4709-872c-e93881d251d4',
            name: 'Второй плейлист',
        },
        {
            id: 'b291457a-e1db-447a-8092-913d518d6b63',
            name: 'Третий плейлист',
        },
        {
            id: '8c37b4f6-ce75-4eed-87a6-45df8277cae1',
            name: 'Четвертый плейлист',
        },
    ];

    const {hostId} = useOutletContext<{hostId: string}>();
    const location = useLocation();

    const [
        playlists,
        setPlaylists
    ] = React.useState<IScheduler[] | null>(null)

    React.useEffect(() => {
        fetchPlaylists(hostId)
            .then(data => setPlaylists(data))
    }, [])

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [form] = Form.useForm();

    const showModal = () => {
        setIsModalVisible(true);
    };

    const [media, setMedia] = useState([])

    // useEffect(() => {
    //     fetchMedia(hostId)
    //         .then((data) => setMedia(data))
    // }, []);

    const handleOk = () => {
        form
            .validateFields()
            .then(values => {
                postScheduler(hostId, values)
                    .then(data => {
                        setIsModalVisible(false);
                        if (data) {
                            setPlaylists(prev => prev ? [...prev, data] : [data])
                        }
                        form.resetFields();
                    })
                    .catch(() => {});
            })
            .catch(() => {});
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Typography.Title level={2}>Плейлисты</Typography.Title>
            <Row justify="space-between" align="middle">
                <Button icon={<PlusOutlined />} onClick={showModal} >Создать</Button>
                <Modal title="Создание плейлиста" open={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <Form form={form} layout="vertical" name="form_in_modal">
                        <Form.Item
                            name="name"
                            label="Имя плейлиста"
                            rules={[{ required: true, message: 'Укажите имя плейлиста' }]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item>

                        </Form.Item>
                    </Form>
                </Modal>
                <Input.Search placeholder="Имя плейлиста ..." style={{ width: 200 }} />

            </Row>
            <List
                pagination={{ position: 'bottom', align: 'start' }}
                dataSource={playlists ?? []}
                size="large"
                renderItem={(item, index) => (
                    <List.Item>
                        <Link to={`/playlist/${item.id}`}>
                            <Typography.Text>{item.name}</Typography.Text>
                        </Link>
                    </List.Item>
                )}
            />
        </>
    )
}

const MediaPicker = () => {


    return (
        <div></div>
    )
}

export default PlaylistsList
