import React from 'react'
import {Link, useParams} from "react-router-dom";
import {Breadcrumb, Button, Card, List, Row, Space, Tag, Tooltip, Typography} from "antd";
import {
    ClockCircleOutlined,
    DeleteOutlined,
    DownOutlined,
    EditOutlined,
    PlusOutlined,
    UpOutlined
} from "@ant-design/icons";
import formatTime from "../../../utils/formatTime";
import {getPlaceholderImage} from "../../../utils/getPlaceholderImage";

const Playlist = () => {
    const { id } = useParams();

    const data = {
        name: "Первый плейлист",
        media: [
            {
                id: '4a4499c5-befa-42c5-8745-52b96bb41e6f',
                name: 'Аргентина',
                preview: 'https://traveltimes.ru/wp-content/uploads/2021/05/86a21cadc98d303d85687.jpg',
            },
            {
                id: 'd8e7f6a5-4b3c-2d1e-0f9a-8b7c6d5e4f3a',
                name: 'Путешествие по горам',
                preview: 'https://sportishka.com/uploads/posts/2022-03/1646813334_1-sportishka-com-p-perito-moreno-argentina-turizm-krasivo-fot-1.jpg',
                duration: 450,
            },
            {
                id: '9a8b7c6d-5e4f-3a2b-1c0d-9e8f7a6b5c4d',
                name: 'Утренняя пробежка',
                preview: 'https://sportishka.com/uploads/posts/2023-06/1687241737_sportishka-com-p-probezhka-v-parke-vkontakte-4.jpg',
                duration: 360,
            }
        ]
    }

    return (
        <>
            <Breadcrumb
                items={[
                    { title: <Link to={'/playlist/'}>Плейлисты</Link> },
                    { title: `${data.name}` },
                ]}
            />
            <Typography.Title editable>{data.name}</Typography.Title>
            <Button icon={<PlusOutlined />} >Добавить медиа</Button>
            <List
                pagination={{ position: 'bottom', align: 'start' }}
                dataSource={data.media}
                size="large"
                itemLayout="vertical"
                renderItem={(item, index) => (
                    <List.Item
                        key={item.id}
                        extra={
                            <div style={{height: '150px', position: 'relative', borderRadius: '10px', overflow: 'hidden'}}>
                                <img
                                    alt="preview"
                                    src={item.preview || getPlaceholderImage(item.id)}
                                    style={{width: '100%', height: '100%', objectFit: 'cover'}}/>

                                {item.duration && (
                                    <Tag icon={<ClockCircleOutlined/>} color="default" style={{
                                        position: 'absolute',
                                        right: '10px',
                                        bottom: '10px'
                                    }}>
                                        {formatTime(item.duration)}
                                    </Tag>
                                )}
                            </div>
                        }
                        actions={[
                            <Button type={'text'} size={'small'}>
                                <Typography.Text type={'secondary'}><UpOutlined/></Typography.Text>
                            </Button>,
                            <Button type={'text'} size={'small'}>
                                <Typography.Text type={'secondary'}><DownOutlined /></Typography.Text>
                            </Button>,
                            <Button type={'text'} size={'small'}>
                                <Typography.Text type={'danger'}><DeleteOutlined/></Typography.Text>
                            </Button>,
                        ]}
                    >
                        <List.Item.Meta
                            title={(<Link to={`/playlists/${item}`}>{item.name}</Link>)}
                        />
                    </List.Item>
                )}
            />
        </>
    )
}


export default Playlist
