import {fetchWithRetry, StatusSetter} from "../../../utils/fetchWIthRetry";

const base_url = process.env.REACT_APP_SERVER_URL

export interface IPlaylist {
    id: string,
    name: string
}

export async function fetchPlaylists(hostId: string): Promise<IPlaylist[] | null> {
    const qp = new URLSearchParams({
        host: hostId
    })
    return await fetchWithRetry<IPlaylist[]>(`${base_url}/api/playlists?${qp.toString()}`)
}

export async function postPlaylist(hostId: string, values: any) {
    const qp = new URLSearchParams({
        host: hostId
    })
    return fetchWithRetry<IPlaylist>(`${base_url}/api/playlists?${qp.toString()}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
    })
}
