import {fetchWithRetry, StatusSetter} from "../../../utils/fetchWIthRetry";

const base_url = process.env.REACT_APP_SERVER_URL

export interface IMedia {
    id: string,
    name: string,
    duration?: number,
    preview?: string,
    creationDate: string
}

export async function fetchMedia(hostId: string): Promise<IMedia[] | null> {
    const qp = new URLSearchParams({
        host: hostId
    })
    return await fetchWithRetry<IMedia[]>(`${base_url}/api/media?${qp.toString()}`)
}


export async function fetchThumb(hostId: string, mediaId: string): Promise<string> {
    const qp = new URLSearchParams({
        host: hostId,
        id: mediaId
    })
    // @ts-ignore
    return await fetchWithRetry(`${base_url}/api/mediathumb?${qp.toString()}`,
        { credentials: 'include'})
}
