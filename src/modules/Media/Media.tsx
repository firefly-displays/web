import React, {useEffect, useState} from 'react'
import {Button, Col, Divider, Input, Row, Select, Space, Typography} from 'antd';
import MediaCard from "./components/MediaCard";
import UploadMedia from "./components/UploadMedia";
import UploadFile from "./components/UploadFile";
import {fetchMedia, IMedia} from "./api";
import {useOutletContext} from "react-router-dom";

const Media = () => {
    const [media, setMedia] = useState<IMedia[]>([])
    const {hostId} = useOutletContext<{hostId: string}>();

    useEffect(() => {
        fetchMedia(hostId)
            .then(data => {
                if (data) setMedia(data)
            })
    }, []);

    return (
        <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
            <Typography.Title level={2}>Медиа</Typography.Title>
            <UploadFile />
            <UploadMedia />
            <Divider />
            <Row justify="space-between" align="middle">
                <Space>
                    Сортировка:
                    <Select
                        defaultValue="default"
                        style={{ width: 200 }}
                        // loading
                        options={[
                            { value: 'default', label: 'По умолчанию' },
                            { value: 'date-desc', label: 'По дате ↓' },
                            { value: 'date-asc', label: 'По дате ↑' },
                            { value: 'duration-desc', label: 'По продолжительности ↓' },
                            { value: 'duration-asc', label: 'По продолжительности ↑' },
                        ]}
                    />
                </Space>
                <Input.Search placeholder="Поиск файла ..." style={{ width: 200 }} />
            </Row>
            <Row gutter={[16, 16]}>
                {media.map((media, index) => (
                    <Col key={index} xs={24} sm={24} md={24} lg={12} xl={12}>
                        <MediaCard {...media}/>
                    </Col>
                ))}
            </Row>
        </Space>
    )
}

export default Media
