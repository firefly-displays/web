import React, {useEffect, useState} from 'react'
import {Button, Card, Modal, Tag, Tooltip} from "antd";
import {
    EditOutlined,
    ClockCircleOutlined
} from '@ant-design/icons';
import formatTime from "../../../utils/formatTime";
import {getPlaceholderImage} from "../../../utils/getPlaceholderImage";
import {fetchThumb, IMedia} from "../api";
import {useOutletContext} from "react-router-dom";

const MediaCard = (props: IMedia) => {
    const [isModalOpen, setIsModalOpen] = React.useState(false);

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const {hostId} = useOutletContext<{hostId: string}>();
    const [img, setImg] = useState('')

    useEffect(() => {
        fetchThumb(hostId, props.id)
            // @ts-ignore
            .then(res => res.text())
            .then(res => setImg(res))
    }, []);

    return (
        <>
            <Card
                cover={
                    <div style={{height: '150px', width: '100%', position: 'relative'}}>
                        <img
                            alt="preview"
                            src={!img ? getPlaceholderImage(props.id) : `data:image/png;base64, ${img}`}
                            style={{width: '100%', height: '100%', objectFit: 'cover'}}/>

                        {props.duration && (
                            <Tag icon={<ClockCircleOutlined />} color="default" style={{
                                position: 'absolute',
                                right: '10px',
                                bottom: '10px'
                            }}>
                                {formatTime(props.duration)}
                            </Tag>
                        )}
                    </div>
                }
                hoverable
                onClick={showModal}
            >
                <Card.Meta
                    title={(<>
                        {props.name}
                        <Tooltip placement="top" title={'Редактировать имя файла'} arrow={true}>
                            <Button size={'small'} type={'text'} shape="circle" icon={ <EditOutlined /> } />
                        </Tooltip>
                    </>)}
                    description={props.creationDate}
                />
            </Card>
            <Modal
                // title="Basic Modal"
                centered
                open={isModalOpen}
                onOk={handleOk}
                onCancel={handleCancel}
                width={1000}
            >
                <p>Some contents...</p>
                <p>Some contents...</p>
                <p>Some contents...</p>
            </Modal>
        </>
    )
}

export default MediaCard
