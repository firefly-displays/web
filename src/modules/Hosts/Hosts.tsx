import ConnectHost from "./components/ConnectHost";
import React, {useState} from "react";
import {fetchWithRetry, LoadingStatus} from "../../utils/fetchWIthRetry";
import {Host} from "./types";
import HostsList from "./components/HostsList";
import {Alert, Space, Spin} from "antd";

const Hosts = () => {
    const [hostLoading, setHostsLoading] = useState<LoadingStatus>('idle')
    const [hosts, setHosts] = useState<Host[]>([])

    React.useEffect(() => {
        fetchWithRetry<Host[]>(`${process.env.REACT_APP_SERVER_URL}/api/hosts/`, {
            statusSetter:
            setHostsLoading
        })
            .then(res => res && setHosts(res))
    }, [])

    return (
        <div>
            <h1>Хосты</h1>
            <ConnectHost onConnect={() => {}}/>
            <div style={{marginTop: '30px'}}></div>
            {hostLoading === 'ok' && hosts.length > 0 && <HostsList hosts={hosts}/>}
            {hostLoading === 'ok' && hosts.length === 0 && <Alert message="Нет подключенных хостов" type="info" />}
            {hostLoading === 'loading' && <Spin />}
            {hostLoading === 'failed' &&  <Alert message="Не получилось загрузить хосты" type="error" />}
        </div>
    )
}

export default Hosts
