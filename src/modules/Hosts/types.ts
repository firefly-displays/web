export interface Host {
    id: string,
    name: string,
    os: string
}