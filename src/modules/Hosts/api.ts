import { fetchWithRetry } from "../../utils/fetchWIthRetry"

export function connectHost(hostId: string) {
    return fetchWithRetry(`${process.env.REACT_APP_SERVER_URL}/api/connectHost`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({code: hostId}),
    })
        // .then((res) => {
        //     if (!res.ok) throw new Error()
        // })
}
