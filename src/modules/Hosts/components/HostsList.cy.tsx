import React from 'react'
import HostsList from './HostsList'

describe('<HostsList />', () => {
  it('renders', () => {
    // see: https://on.cypress.io/mounting-react
    cy.mount(<HostsList hosts={[
      {
        id: "123",
        name: "Host 1",
        os: "Windows"
      },
      {
        id: "123",
        name: "Host 2",
        os: "Windows"
      }
    ]} />)
  })
})
