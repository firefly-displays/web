import {Host} from "../types";
import { ProList } from '@ant-design/pro-components';
import {Link, useNavigate} from "react-router-dom";
import { Card } from "antd";


const HostsList: React.FC<{ hosts: Host[] }> = ({hosts}) => {
    const navigate = useNavigate()

    return (
        <ProList<Host>
            rowKey="id"
            dataSource={hosts}
            showActions="hover"
            metas={{
                title: {
                    dataIndex: 'name',

                },
                description: {
                    dataIndex: 'os',
                },
                actions: {
                    render: (text, row, index, action) => [
                        <Link to={`./${row.id}/`} relative={"path"}>Настройки</Link>,
                    ],
                }
            }}
        />
    );
}

export default HostsList
