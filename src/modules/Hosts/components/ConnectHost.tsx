import React, { useState } from 'react';
import {Button, Form, Input, Modal} from 'antd';
import {connectHost} from "../api";

export interface IConnectHostProps {
    onConnect?: () => void;
}

const ConnectHost: React.FC<IConnectHostProps> = (props) => {
    const [open, setOpen] = useState(false);
    const [confirmLoading, setConfirmLoading] = useState(false);
    const [modalText, setModalText] = useState('');

    const showModal = () => {
        setOpen(true);
    };

    const handleCancel = () => {
        setOpen(false);
    };

    const onFinish = (values: any) => {
        setConfirmLoading(true)
        setConfirmLoading(true);
        connectHost(values.hostId)
            .then(() => {
                setOpen(false);
                setConfirmLoading(false);
                props.onConnect && props.onConnect()
            })
            .catch(() => {
                setConfirmLoading(false);
                setModalText("Ошибка")
            })
    };

    return (
        <>
            <Button onClick={showModal}>
                Подключить хост
            </Button>
            <Modal
                title="Подлючение хоста"
                open={open}
                confirmLoading={confirmLoading}
                onCancel={handleCancel}
                footer={[
                    <Button onClick={handleCancel} key={"cancel"}>Отмена</Button>,
                    <Button form="hostIdForm" key="submit" htmlType="submit" type={"primary"} loading={confirmLoading}>
                        Подключиться
                    </Button>
                ]}
            >
                {modalText}
                <Form id="hostIdForm" onFinish={onFinish}>
                    <Form.Item
                        name="hostId"
                        rules={[{ required: true, message: 'Пожалуйста, введите id хоста!' }]}
                    >
                        <Input placeholder="Введите id хоста" />
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
};

export default ConnectHost;