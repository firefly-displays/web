export function getGoogleUrl() {
    const rootUrl = 'https://accounts.google.com/o/oauth2/v2/auth'

    //@ts-ignore
    const params = new URLSearchParams({
        redirect_uri: process.env.REACT_APP_PUBLIC_GOOGLE_OAUTH_REDIRECT_URL,
        client_id: process.env.REACT_APP_PUBLIC_GOOGLE_CLIENT_ID,
        access_type: 'offline',
        response_type: 'code',
        prompt: 'consent',
        scope: [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ].join(' ')
    })

    return `${rootUrl}?${params.toString()}`
}