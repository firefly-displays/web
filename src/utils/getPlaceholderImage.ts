export function getPlaceholderImage(id: string) {
    return `https://generative-placeholders.glitch.me/image?width=600&height=300&img=01&solt=${id}`
}