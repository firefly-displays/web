import { cookieParser } from "./cookieParser";

export type LoadingStatus = 'idle' | 'loading' | 'ok' | 'failed';
export type StatusSetter = React.Dispatch<React.SetStateAction<LoadingStatus>>
export const fetchWithRetry = async <Type>(
    url: URL | string,
    options?: RequestInit & {statusSetter?: StatusSetter},
    retries = 1
) : Promise<Type|null> => {
    const statusSetter = options?.statusSetter

    try {
        statusSetter && statusSetter('loading')
        const response = await fetch(url, {
            ...options,
            credentials: 'include',
            headers: {
                ...options?.headers,
                Authorization: `Bearer ${cookieParser(document.cookie)['accessToken']}`,
                'x-refresh': cookieParser(document.cookie)['refreshToken']
            }
        });
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        statusSetter && statusSetter('ok')
        return await response.json() as Type;
    } catch (err) {
        if (retries > 0) {
            return fetchWithRetry<Type>(url, options, retries - 1);
        }
        statusSetter && statusSetter('failed')
        return null
    }
}
