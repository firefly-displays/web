import React from 'react'
import {Button, Card, Layout} from "antd";
import Header from "../components/Header/Header";
import {Link} from "react-router-dom";
import {useAuth} from "../../hooks/useAuth";

const Landing = () => {
    const { user } = useAuth();

    return (
        <Layout style={{ minHeight: '100vh' }}>
            <Layout.Header style={{ background: 'fff' }}>
                <Header>
                    {user && (<Button><Link to={'/dashboard'}>Панель управления</Link></Button>)}
                </Header>
            </Layout.Header>
            <Layout.Content className={'container'} style={{ margin: '20px auto' }}>
                <Card style={{ background: '#fff', minHeight: '100%' }}>
                    TODO
                </Card>
            </Layout.Content>
        </Layout>
    )
}

export default Landing
