import React, {ReactNode} from 'react'
import {Col, Row, Space} from "antd";
import {Link} from "react-router-dom";
import logo from "./assets/logo.svg";
import User from "./User";

const Header = (props: {children?: ReactNode}) => {
    return (
        <Row className={'container'}>
            <Col span={6} style={{ display: 'flex', alignItems: 'center' }}>
                <Link to="/" style={{ display: 'flex', alignItems: 'center' }}>
                    <img src={logo} alt="logo" style={{width: '40px', height: '40px' }}/>
                </Link>
            </Col>
            <Col span={18} style={{ textAlign: 'right' }}>
                <Space size={'large'}>
                    {props.children}
                    <User />
                </Space>
            </Col>
        </Row>
    )
}

export default Header