import React from 'react'
import {UserOutlined} from "@ant-design/icons";
import {Avatar, Button, Popover} from "antd";
import {useAuth} from "../../../hooks/useAuth";

const User = () => {
    const { user, signIn, signOut } = useAuth()

    return (
        <>
            {!user ? (
                <Avatar onClick={signIn} style={{cursor: 'pointer'}} icon={<UserOutlined />} />
            ) : (
                <Popover
                    placement="bottom"
                    title={user.name}
                    content={<Button type={'link'} onClick={signOut}>Выйти</Button>}
                    trigger="click"
                >
                    <Avatar src={user.picture} style={{cursor: "pointer"}} />
                </Popover>
            )}
        </>
    )
}

export default User
