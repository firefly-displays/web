import React from "react";
import {AuthContext, IUser} from '../../hooks/useAuth';
import {getGoogleUrl} from "../../utils/getGoogleUrl";
import {fetchWithRetry} from "../../utils/fetchWIthRetry";
import {jwtDecode} from 'jwt-decode';

export const AuthProvider = ({ children }: any) => {
    const getAccessToken = () => {
        return document.cookie
            .split('; ')
            .find(row => row.startsWith('accessToken='))
            ?.split('=')[1];
    }

    const getUserFromCookies = (): IUser|null => {
        const accessToken = getAccessToken()

        if (accessToken) {
            return jwtDecode<IUser>(accessToken)
        }

        return null
    }

    const [user, setUser] = React.useState<IUser|null>(getUserFromCookies())
    const [accessToken] = React.useState<string>(getAccessToken() || '')

    React.useEffect(() => {
        getUser()
    }, [])

    const getUser = () => {
        fetchWithRetry<IUser>(`${process.env.REACT_APP_SERVER_URL}/api/me`)
            .catch(_ => {})
            .then(res => setUser(res as IUser))
    }

    const signIn = () => {
        const url = getGoogleUrl();
        const windowFeatures = 'width=800,height=600,resizable=yes,scrollbars=yes';
        window.open(url, '_blank', windowFeatures);

        const listener = (event: any) => {
            if (event.origin !== process.env.REACT_APP_SERVER_URL) {
                return;
            }

            getUser()

            window.removeEventListener('message', listener)
        };

        window.addEventListener('message', listener)
    }

    const signOut = () => {
        fetchWithRetry<any>(`${process.env.REACT_APP_SERVER_URL}/api/logout`)
            .catch(_ => {})
            .then(_ => setUser(null))
    }

    return (
        <AuthContext.Provider value={{ user, signIn, signOut, accessToken }}>
            {children}
        </AuthContext.Provider>
    );
};
