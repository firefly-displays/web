import React, {ReactNode} from 'react'
import {useAuth} from "../../hooks/useAuth";
import {useNavigate} from "react-router-dom";

const AuthOnly = (props: { children: ReactNode }) => {
    const { user } = useAuth()
    const navigate = useNavigate()

    React.useEffect(() => {
        const navigateHomeTimeout = setTimeout(() => {
            !user && navigate('/')
        }, 1000)
        return () => clearTimeout(navigateHomeTimeout)
    }, [user])

    return (<>{ props.children }</>)
}

export default AuthOnly
