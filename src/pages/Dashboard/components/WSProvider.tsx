import React, {createContext, ReactNode, useContext, useEffect, useRef, useState} from 'react';
export interface IWSContext {
    addListener: (callback: (msg: string) => void) => void,
    removeListener: (callback: (msg: string) => void) => void,
    sendMessage: (msg: string) => void,
}
const WSContext = createContext<IWSContext>({
    addListener: () => {},
    removeListener: () => {},
    sendMessage: () => {},
});

export interface IWSProviderProps { url: string, children: ReactNode }
export type OnMessageCallback = (msg: string) => void


export const WSProvider = ({ url, children }: IWSProviderProps) => {
    const [listeners, setListeners] = useState<OnMessageCallback[]>([])
    const ws = useRef<WebSocket|null>(null);

    const addListener = (callback: OnMessageCallback) => {
        setListeners(prev => [...prev, callback])
    }

    useEffect(() => {
        console.log('listeners changed')
        console.log(listeners)
    }, [listeners]);

    const removeListener = (callback: OnMessageCallback) => {
        setListeners(prev => prev.filter(l => l!=callback))
    }

    useEffect(() => {
        const initWS = () => {
            ws.current = new WebSocket(url);

            ws.current.onopen = () => {
                console.log('WebSocket connected');
            };

            ws.current.onmessage = (event) => {
                listeners.forEach(l => l(event.data))
            };

            ws.current.onclose = (event) => {
                console.log('WebSocket rip, trying to reconnect...');
                setTimeout(() => {
                    initWS()
                }, 1000);
            };
        }
        initWS()

        return () => {
            if (ws.current) {
                ws.current.onclose = (event) => {};
                ws.current?.close();
            }
        };
    }, [url, listeners]);

    const sendMessage = (message: string) => {
        if (ws.current?.readyState === WebSocket.OPEN) {
            ws.current?.send(message);
        }
    };

    const value = {
        addListener,
        removeListener,
        sendMessage,
    };

    return (
        <WSContext.Provider value={value}>
            {children}
        </WSContext.Provider>
    );
};

export const useWebSocket = () => {
    const context = useContext(WSContext);
    if (context === null) {
        throw new Error('useWebSocket must be used within a WSProvider');
    }
    return context;
};

// <Modal
//     title="Упс"
//     open={openReconnect}
//     footer={[
//         <Button onClick={() => navigate('/dashboard')} key={"cancel"}>К списку хостов</Button>,
//         <Button onClick={() => setWS(connectWS())}
//                 key="reconnect"
//                 type={"primary"}
//                 loading={reconnectLoading}
//         >
//             Переподключиться</Button>
//     ]}
// >
//     <p>Подключение с хостом разорвано</p>
// </Modal>
