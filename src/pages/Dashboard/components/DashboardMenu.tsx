import {Link, useLocation} from "react-router-dom";
import {Menu} from "antd";


function DashboardMenu() {
    const location = useLocation();

    const menu = [
        {
            title: 'Дисплеи',
            path: ''
        },
        {
            title: 'Планировщики',
            path: 'scheduler'
        },
        {
            title: 'Плейлисты',
            path: 'playlist'
        },
        {
            title: 'Медиа',
            path: 'media'
        }
    ];

    const getDefaultSelectedKeys = () => {
        return [`${menu.findIndex(item => item.path === (location.pathname+'/').split('/')[3]) || 0}`];
    };

    return (
        <Menu theme="light" mode="vertical" defaultSelectedKeys={getDefaultSelectedKeys()}>
            {menu.map((item, i) => (
                <Menu.Item key={i}><Link to={item.path}>{item.title}</Link></Menu.Item>
            ))}
        </Menu>
    );
}

export default DashboardMenu;

