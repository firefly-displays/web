import React, {useCallback, useState} from 'react'
import {Button, Card, Col, Layout, Menu, Modal, Row} from 'antd'
import {Outlet, useLocation, useNavigate, useParams} from 'react-router-dom';
import Header from "../components/Header/Header";
import AuthOnly from "../components/AuthOnly";
import DashboardMenu from "./components/DashboardMenu";
import {useAuth} from "../../hooks/useAuth";
import {WSProvider} from "./components/WSProvider";

const Dashboard = () => {
    const auth = useAuth();
    const { id } = useParams();

    return (
        <AuthOnly>
            <WSProvider url={`ws://token:${auth.accessToken}@${process.env.REACT_APP_SERVER_DOMAIN}/client?hostId=${id}`}>
                <Layout style={{ minHeight: '100vh' }}>
                    <Layout.Header style={{ background: 'fff' }}>
                        <Header />
                    </Layout.Header>
                    <Layout.Content className={'container'} style={{ margin: '20px auto' }}>
                        <Card style={{ background: '#fff', minHeight: '100%' }}>
                            <Row>
                                <Col span={6}><DashboardMenu /></Col>
                                <Col span={18}><Card style={{ margin: "0 15px" }}>
                                    <Outlet context={{hostId: id}}/>
                                </Card></Col>
                            </Row>
                        </Card>
                    </Layout.Content>
                </Layout>
            </WSProvider>
        </AuthOnly>
    );
};

export default Dashboard;