import {Card, Layout} from "antd";
import Header from "../components/Header/Header";
import AuthOnly from "../components/AuthOnly";
import React from "react";
import {Hosts} from "../../modules";

const HostsPage = () => {
    return (
        <AuthOnly>
            <Layout style={{ minHeight: '100vh' }}>
                <Layout.Header style={{ background: 'fff' }}>
                    <Header />
                </Layout.Header>
                <Layout.Content className={'container'} style={{ margin: '20px auto' }}>
                    <Card style={{ background: '#fff', minHeight: '100%' }}>
                        <Hosts/>
                    </Card>
                </Layout.Content>
            </Layout>
        </AuthOnly>
    )
}

export default HostsPage