import React, { createContext, useState, useContext, useEffect } from 'react';

export interface IUser {
    id: string,
    name: string,
    picture: string
}

export interface IAuthContext {
    user: IUser|null,
    signIn: () => void
    signOut: () => void
    accessToken: string
}
export const AuthContext = createContext<IAuthContext>({
    user: null,
    signIn: () => {},
    signOut: () => {},
    accessToken: ''
});

export const useAuth = () => useContext(AuthContext);