import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {Dashboard, HostsPage, Landing} from "./pages";
import {Displays, Media, Playlist, Playlists, Scheduler, Schedulers, SchedulersList, PlaylistsList} from "./modules";

function App() {
  return (
      <BrowserRouter>
          <Routes>
              <Route path="/" element={<Landing />}/>
              <Route path="/dashboard" element={<HostsPage />} />
              <Route path="/dashboard/:id/" element={<Dashboard />} >
                  <Route path="/dashboard/:id/" element={<Displays />} />
                  <Route path="/dashboard/:id/scheduler" element={<Schedulers />}>
                      <Route path="/dashboard/:id/scheduler/" element={<SchedulersList />}/>
                      <Route path="/dashboard/:id/scheduler/:id" element={<Scheduler />}/>
                  </Route>
                  <Route path="/dashboard/:id/playlist" element={<Playlists />} >
                      <Route path="/dashboard/:id/playlist/" element={<PlaylistsList />}/>
                      <Route path="/dashboard/:id/playlist/:id" element={<Playlist />}/>
                  </Route>
                  <Route path="/dashboard/:id/media" element={<Media />} />
              </Route>

          </Routes>
      </BrowserRouter>
  );
}

export default App;
