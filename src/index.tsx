import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {ConfigProvider} from "antd";
import {AuthProvider} from "./pages/components/AuthProvider";

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  // <React.StrictMode>
      <ConfigProvider
          theme={{
              components: {
                  Layout: {
                      headerBg: '#fff',
                      siderBg: '#fff'
                  },
              },
          }}
      >
        <AuthProvider>
            <App />
        </AuthProvider>
      </ConfigProvider>
  // </React.StrictMode>
);
